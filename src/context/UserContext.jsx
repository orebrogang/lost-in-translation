import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utility/storage";



const UserContext = createContext()
// Function to check User Context
export const useUser = () => {
    return useContext(UserContext) // {user, setUser}
}


//Provider -> state managing

const UserProvider = ({children}) => {
 
 const [user, setUser] = useState(storageRead(STORAGE_KEY_USER))
 
 const state = {
    user,
    setUser
 }


    return (
        <UserContext.Provider value={state}>
        {children}
        </UserContext.Provider>
    )
}

export default UserProvider