import UserProvider from "./UserContext";
// Our app context 
const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};

export default AppContext;
