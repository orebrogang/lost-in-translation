import { createHeaders } from "."

const apiURL = process.env.REACT_APP_API_URL;
// Adds a sentence to translation list
export const addTranslation = async(user, translation) => {
    try {
        const response = await fetch(`${apiURL}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                username: user.username,
                translations: [...user.translations, translation]
            })
        })

        if(!response.ok) {
            throw new Error('Could not add translation')
        }
        const result = await response.json()
        return [null, result]
    } catch (error) {
        return [ error.message, null]
    }
}

// Clears the translation list
export const translationClearHistory = async (userId) => {
  try {
    const response = await fetch(`${apiURL}/${userId}`, {
        method: 'PATCH',
        headers: createHeaders(),
        body: JSON.stringify({
            translations: []
        })
    })
    if(!response.ok) {
        throw new Error('Could not update translations')
    }
    const result = await response.json()
    return [null, result]
  } catch (error) {
    return [error.message, null]
  }
}