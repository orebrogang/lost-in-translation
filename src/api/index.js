const apiKey = process.env.REACT_APP_API_KEY
// Our custom header for Api calls
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}