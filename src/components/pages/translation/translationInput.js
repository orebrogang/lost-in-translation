import { useState } from "react";
import { useForm } from "react-hook-form";
// Input form that stores data from user inputs
const TranslationInput = ({ onTranslation }) => {
  const { register, handleSubmit } = useForm();
  const [message, setMessage] = useState('')

  const handleChange = e => {
    const result = e.target.value.replace(/[^a-z]/gi, '')
    setMessage(result)
  }

  const onSubmit = ({ translationInput }) => {
    onTranslation(translationInput);
  };




  return (
    <div container-translation>
      <form className="translation-form" onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <label className="label-translation" htmlFor="translations"><h1>Translations</h1></label>
          <div className="mb-3">
            <input maxLength={20} className="input-translation" type="text" {...register("translationInput")} value={message}
        onChange={handleChange}/>
          </div>
          <button disabled={!message} className="btn btn-primary btn-lg btn-color" type="submit">
            Translate
          </button>
        </fieldset>
      </form>
    </div>
  );
};

export default TranslationInput;
