const TranslationImages = (props) => {
    return(
        <img src={props.src} alt="sign-icons" width="8%" />
    )
}

export default TranslationImages;
