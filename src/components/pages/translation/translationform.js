import TranslationImages from "./translationsimages";
// Component that render sign language images
const TranslationForm = props =>  {
    return(
        <div className="letter-div">
            {props.letters.map((letter, index) => {
                if(letter.match(/\w/)){

                    return <TranslationImages src={`./assets/individial_signs/${letter}.png`} key={index} />
                }

                if(letter === " ") {
                    return <textarea cols="3" className="invisible" key={index} />
                }

                return <textarea hidden key={index} />
            })}
        </div>
    )
}

export default TranslationForm;
