// Child component for each image
const TranslationImages = (props) => {
    return(
        <img src={props.src} alt="" />
    )
}

export default TranslationImages;