import withAuth from "../../../hoc/withAuth";
import TranslationInput from "./translationInput";
import { useUser } from "../../../context/UserContext";
import "./translation-page.css";
import { addTranslation } from "../../../api/translations";
import { storageSave } from "../../../utility/storage";
import { STORAGE_KEY_USER } from "../../../const/storageKeys";
import { useState } from "react";
import TranslationForm from "./translationform";
// Translations component
const Translations = () => {
  const { user, setUser } = useUser();
  const [word, setWord] = useState({
      word: '',
      letters: []
  });

  // Handles translation input
  const handleTranslateClick = async (translation) => {
    const translationString = translation;

    const [error, updatedUser] = await addTranslation(user, translationString);

    if (error !== null) {
      return;
    }

    setWord({
      letters: translationString.toLowerCase().split("")
    });

    //Keep UI state and Server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser);
    //Update context state.
    setUser(updatedUser);

    console.log("Error", error);
    console.log("Result", updatedUser);
  };

  return (
    <>
      <section id="translationInput">
        <TranslationInput onTranslation={handleTranslateClick} />
        <div className="output-container">
          <div className="letter-div">
            <TranslationForm letters={word.letters} />
          </div>
        </div>
      </section>
    </>
  );
};

export default withAuth(Translations);
