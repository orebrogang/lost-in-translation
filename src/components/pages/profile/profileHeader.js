// Component that renders our header
const ProfileHeader = ({ username }) => {
  return (
    <div className="profile-header">
      <header>
        <h3>Welcome back {username}!</h3>
      </header>
    </div>
  );
};

export default ProfileHeader;
