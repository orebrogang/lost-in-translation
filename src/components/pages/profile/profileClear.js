import { translationClearHistory } from "../../../api/translations";
import { STORAGE_KEY_USER } from "../../../const/storageKeys";
import { useUser } from "../../../context/UserContext";
import { storageClear, storageSave } from "../../../utility/storage";

const ProfileClear = () => {
  const { user, setUser } = useUser();
// Logout function
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure you want to logout?")) {
      storageClear(STORAGE_KEY_USER);
      setUser(null);
    }
  };
// Clear translation function
  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?")) {
      return;
    }
    const [clearError] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }

    const updatedUser = {
      ...user,
      translations: [],
    };

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  return (
    <div className="action-button">
     
          <button onClick={handleClearHistoryClick} className="clearButton btn-danger btn-md">
            Clear translations
          </button>
          <button onClick={handleLogoutClick} className="logoutButton btn-danger btn-md">
            Logout
          </button>
      
    </div>
  );
};

export default ProfileClear;
