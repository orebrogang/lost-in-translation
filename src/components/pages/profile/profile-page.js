import React, { useEffect } from "react";
import { userById } from "../../../api/user";
import { STORAGE_KEY_USER } from "../../../const/storageKeys";
import { useUser } from "../../../context/UserContext";
import withAuth from "../../../hoc/withAuth";
import { storageSave } from "../../../utility/storage";
import ProfileClear from "./profileClear";
import ProfileHeader from "./profileHeader";
import ProfileHistory from "./profileHistory";
import "./profile-page.css"
// Renders the profile page
const ProfilePage = () => {

  const {user, setUser} = useUser()

  useEffect(() => {
   const findUser = async () => {
    const [error, latestUser] = await userById(user.id)
    if(error === null) {
      storageSave(STORAGE_KEY_USER, latestUser)
      setUser(latestUser)

    }
   }
 
  }, [setUser, user.id])

  return (
    <>
      <ProfileHeader username={user.username}/>
      <ProfileHistory translations={user.translations}/>
      <ProfileClear/>
    </>
  );
};

export default withAuth(ProfilePage);
