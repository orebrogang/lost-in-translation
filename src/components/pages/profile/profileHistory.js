import ProfileOrderHistoryItem from "./ProfileOrderHistoryItem";
// Generates the translation from out API
const ProfileHistory = ({ translations }) => {
  const translationList = translations.map((translations, index) => (
    <ProfileOrderHistoryItem
      key={index + "-" + translations}
      translations={translations}
    />
  ));

  return (
    <div className="profile-history">
        <div className="profile-history-container">
        <section>
          <h4 className="hfour">Your translation history</h4>
          {translationList.length === 0 && (
            <p>You have not translations at the moment!</p>
          )}
          <ul className="ul-list">{translationList}</ul>
        </section>
      </div>
    </div>
  );
};

export default ProfileHistory;
