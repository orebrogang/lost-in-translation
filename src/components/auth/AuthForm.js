import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
// import { loginUser } from "../api/user";
import { storageRead, storageSave } from "../../utility/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import "./Auth-Form.css";
// Basic validation
const usernameConfig = {
  required: true,
  minLength: 4,
};

const AuthForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  //Local Storage
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  //Side Effect

  useEffect(() => {
    if (user !== null) {
      navigate("/translator");
    }
    console.log("User has changed!", user);
    //redirect
  }, [user, navigate]);

  //Event Handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  //Render Functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }

    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }

    if (errors.username.type === "minLength") {
      return <span>User is to short(min 3)</span>;
    }
  })();

  return (
    <div className="login-form form-outline mb-3">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="container-auth">
          <fieldset>
            <label htmlFor="username">
              <h1>Username:</h1>
            </label>
            <div className="mb-3">
              <input
                className="form-control"
                type="text"
                placeholder="username"
                {...register("username", usernameConfig)}
              />
            </div>

            <button
              type="submit"
              className="btn btn-primary btn-lg btn-color"
              disabled={loading}
            >
              Submit
            </button>
            {loading && <p>Welcome!</p>}
            {apiError && <p>{apiError}</p>}
          </fieldset>
          {errorMessage}
        </div>
      </form>
    </div>
  );
};

export default AuthForm;
