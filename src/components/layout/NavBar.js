import "./NavBar.css";
import { Link } from "react-router-dom";
import { useUser } from "../../context/UserContext";
// Navbar component
const NavBar = () => {
  const { user } = useUser();
  return (
    <header className="navbar">
      <div className="navbar__title">Lost In Translations</div>
      {user !== null && (
        <div className="click_item">
          <div className="username-div">{user.username} <img className="img-user" src="vector-icon.jpg" alt="profile-icon" /></div>
          <ul className="navbar__item">
            <li>
              <Link to="/profile">Profile</Link>
            </li>
            <li>
              <Link to="/translator">Translator</Link>
            </li>
          </ul>
        </div>
      )}
    </header>
  );
};
export default NavBar;
