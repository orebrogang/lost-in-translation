const validateKey = (key) => {
  if (!key || typeof key !== "string") {
    throw new Error("Invalid key was provided");
  }
};

export const getStorage = key => {
  const data = sessionStorage.getItem(key);
  if(data){
      return data;
  }
  return false;
}
// Saving the session key
export const storageSave = (key, value) => {
  validateKey(key);

  if (!value) {
    throw new Error("storageSave: No StorageKey provided for " + key);
  }

  sessionStorage.setItem(key, JSON.stringify(value));
};
// Reading from the session key
export const storageRead = (key) => {
  validateKey(key);

  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};
// Clearing the session key
export const storageClear = (key) => {
  validateKey(key);

  sessionStorage.removeItem(key);
};
