import { BrowserRouter, Routes, Route } from "react-router-dom";
import NavBar from "./components/layout/NavBar";
import "./components/layout/NavBar.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import TranslatorPage from "./components/pages/translation/translator-page";
import  ProfilePage from "./components/pages/profile/profile-page";
import Auth from "./components/auth/Auth";
function App() {
  return (
    <div className="App">
      <>
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route exact path="/" element={<Auth />} />
            <Route path="translator" element={<TranslatorPage />} />
            <Route path="profile" element={<ProfilePage />} />
          </Routes>
        </BrowserRouter>
        <footer className="footer">
          <p className="p-tag">Author: Carl, Alexander, Anders</p>
        </footer>
      </>
    </div>
  );
}

export default App;
