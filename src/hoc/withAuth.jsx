import {Navigate} from 'react-router-dom'
import { useUser } from "../context/UserContext"
// Checking for valid user, otherwise sent back to login page
const withAuth = Component => props => {
    const {user} = useUser()
    if(user !== null) {
        return <Component {...props}/>
    }
    else {
        return <Navigate to="/" />
    }
}

export default withAuth